#### Secure I/F

A Module designed to disable data delivery through usb. It can protect your data when you are charging in public area.  
For more infomation, please see [original project](https://github.com/iternull/secure-if).(Chinese only)

#### NOTICE

* You should use latest Magisk Manager to install this module. If you meet any problem under installation from Magisk Manager, please try to install it from recovery.
* Recent fixes:
Magisk v17 Template 17000 compatabilities

#### Credit & Support

* Copyright (C) 2017-2018 simonsmh <simonsmh@gmail.com>
* Any issue or pull request is welcomed.
* Star this module at [GitHub](https://github.com/Magisk-Modules-Repo/magisk-secure-if).
